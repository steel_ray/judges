import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./pages/main/main.module').then(m => m.MainModule)
  },
  {
    path: 'category/:slug',
    loadChildren: () =>
      import('./pages/category/category.module').then(m => m.CategoryModule)
  },
  {
    path: 'library/:slug',
    loadChildren: () =>
      import('./pages/library/library.module').then(m => m.LibraryModule)
  },
  {
    path: 'library/:slug/:tab',
    loadChildren: () =>
      import('./pages/library/library.module').then(m => m.LibraryModule)
  },
  {
    path: 'gallery',
    loadChildren: () =>
      import('./pages/gallery/gallery.module').then(m => m.GalleryModule)
  },
  {
    path: 'gallery/:slug',
    loadChildren: () =>
      import('./pages/gallery-view/gallery-view.module').then(
        m => m.GalleryViewModule
      )
  },
  {
    path: 'post/:slug',
    loadChildren: () =>
      import('./pages/post/post.module').then(m => m.PostModule)
  },
  {
    path: 'search',
    loadChildren: () =>
      import('./pages/search/search.module').then(m => m.SearchModule)
  },
  {
    path: 'sitemap',
    loadChildren: () =>
      import('./pages/sitemap/sitemap.module').then(m => m.SitemapModule)
  },
  {
    path: '**',
    loadChildren: () =>
      import('./pages/not-found/not-found.module').then(m => m.NotFoundModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
