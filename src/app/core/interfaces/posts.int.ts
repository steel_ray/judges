export interface PostsInt {
  title: string;
  description: string;
  slug: string;
  images: [string];
  published_at: number;
  category_title: string;
  author_name: string;
  content?: string;
  document?: string;
  tags?: [];
}
