export interface AlbumInt {
  slug: string;
  title: string;
  thumbnail?: string;
  media?: [
    {
      filename: string;
      title: string;
      url: string;
      thumbnail: string;
    }
  ];
}
