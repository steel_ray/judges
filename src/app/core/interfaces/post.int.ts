export interface PostInt {
  title: string;
  description: string;
  slug: string;
  images: any;
  document: string | boolean;
  published_at: number;
  category: { title: string; slug: string };
  content: string;
  status?: number;
  error?: any;
}
