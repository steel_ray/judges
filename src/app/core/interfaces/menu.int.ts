export interface MenuInt {
  label: string;
  slug?: string;
  view?: string;
  children?: MenuInt[];
  image?: string;
}
