export function groupBy(list, props) {
  return list.reduce((a, b) => {
    (a[b[props]] = a[b[props]] || []).push(b);
    return a;
  }, {});
}
export function onlyUniqueArray(value, index, self) {
  return self.indexOf(value) === index;
}

export function onlyUniqueObject(arr, column) {
  const map$ = new Map();
  const result = [];
  for (const item of arr) {
    if (!map$.has(item[column])) {
      map$.set(item[column], true);
      result.push(item);
    }
  }
  return result;
}

export function searchMenuItems(arr, slug) {
  let current: any = null;
  let parent: any = null;
  let subItems = [];
  current = arr.find(cat => cat.slug === slug);
  if (!current) {
    arr.forEach(cat => {
      if (cat.children) {
        const curr = cat.children.find(i => i.slug === slug);
        if (curr) {
          parent = cat;
          current = curr;
          current.children = cat.children;
        }
      }
    });
  }
  subItems = current.children ? current.children : arr;
  return { subItems, parent, current };
}
