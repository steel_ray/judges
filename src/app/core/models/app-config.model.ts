import { environment } from 'src/environments/environment.prod';

export class AppConfigModel {
  private LANG_CONST = environment.langConst;
  locales: [
    {
      locale: string;
      name: string;
    }
  ];
  messages: [string];
  siteTitle: string;
  siteDescription: string;
  defaultLocale =
    localStorage.getItem(this.LANG_CONST) || environment.appDefaultLang;
  pageSize = environment.pageSize;

  constructor() {
    // init default configs
    if (!localStorage.getItem(this.LANG_CONST)) {
      localStorage.setItem(this.LANG_CONST, this.defaultLocale);
    } else {
      this.defaultLocale = localStorage.getItem(this.LANG_CONST);
    }
  }
}

export class AppSpecialAbs {
  textSpeech: boolean;
  siteTheme: string;
  fontSize: string;
  constructor() {
    this.textSpeech = false;
    this.siteTheme = 'default';
    this.fontSize = null;
  }
}
