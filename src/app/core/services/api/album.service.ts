import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlbumInt } from '../../interfaces/album.int';
import { APP_CONFIG } from '../share/app-config-store.service';
import { publishReplay, refCount } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AlbumService extends ApiService {
  store = [];
  fetchAll(params: {}, cache = true): Observable<AlbumInt[]> {
    let albums = this.http.get<AlbumInt[]>(this.getApiUrl('/albums'), params);

    if (cache) {
      const key = Object.values(params) + APP_CONFIG.data.defaultLocale;
      if (!this.store[key]) {
        albums = albums.pipe(
          publishReplay(1),
          refCount()
        );
        this.store[key] = albums;
      }
      return this.store[key];
    }
    return albums;
  }
  fetchOne(slug: string, cache = true): Observable<AlbumInt> {
    let album = this.http.get<AlbumInt>(this.getApiUrl(`/albums/${slug}`), {
      params: { expand: 'media' }
    });
    if (cache) {
      if (!this.store[slug]) {
        album = album.pipe(
          publishReplay(1),
          refCount()
        );
        this.store[slug] = album;
      }
      return this.store[slug];
    }
    return album;
  }
}
