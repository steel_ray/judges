import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { PostsInt } from '../../interfaces/posts.int';
import { Observable } from 'rxjs';
import { publishReplay, refCount } from 'rxjs/operators';
import { APP_CONFIG } from '../share/app-config-store.service';
import { PostInt } from '../../interfaces/post.int';
@Injectable({
  providedIn: 'root'
})
export class PostService extends ApiService {
  store = [];
  fetchAll(params: any, cache = true, observe: any = null): Observable<any> {
    let posts = this.http.get<PostsInt[]>(this.getApiUrl('/posts'), {
      params,
      observe
    });

    if (cache) {
      const key =
        Object.values(params).join('') + APP_CONFIG.data.defaultLocale;
      if (!this.store[key]) {
        posts = posts.pipe(
          publishReplay(1),
          refCount()
        );
        this.store[key] = posts;
      }
      return this.store[key];
    }
    return posts;
  }

  fetchOne(slug: string, cache = true): Observable<PostInt> {
    let post = this.http.get<PostInt>(this.getApiUrl(`/posts/${slug}`));

    if (cache) {
      const key = slug + APP_CONFIG.data.defaultLocale;
      if (!this.store[key]) {
        post = post.pipe(
          publishReplay(1),
          refCount()
        );
        this.store[key] = post;
      }
      return this.store[key];
    }
    return post;
  }
}
