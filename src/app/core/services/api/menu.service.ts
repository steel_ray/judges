import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MenuInt } from '../../interfaces/menu.int';
import { publishReplay, refCount } from 'rxjs/operators';
import { APP_CONFIG } from '../share/app-config-store.service';
@Injectable({
  providedIn: 'root'
})
export class MenuService extends ApiService {
  items: Observable<MenuInt[]>;
  store = [];

  getItems(slug = 'main-menu'): Observable<MenuInt[]> {
    const key = slug + APP_CONFIG.data.defaultLocale;
    if (!this.store[key]) {
      this.items = this.http
        .get<MenuInt[]>(this.getApiUrl(`/menu-items/${slug}`))
        .pipe(
          publishReplay(1),
          refCount()
        );
      this.store[key] = this.items;
    }
    return this.store[key];
  }

  clearCache() {
    this.items = null;
  }
}
