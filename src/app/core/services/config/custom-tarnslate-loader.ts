import { Injectable } from '@angular/core';
import { TranslateLoader } from '@ngx-translate/core';
import { Observable, Subscriber } from 'rxjs';
import { ApiService } from '../api/api.service';
import { APP_CONFIG } from '../share/app-config-store.service';

@Injectable()
export class CustomTranslateLoader extends ApiService
  implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return Observable.create((observer: Subscriber<any>) => {
      observer.next(APP_CONFIG.data.messages[lang]);
      observer.complete();
    });
  }
}
