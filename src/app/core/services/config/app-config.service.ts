import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { AppConfigModel } from '../../models/app-config.model';
import { APP_CONFIG } from '../share/app-config-store.service';
@Injectable({
  providedIn: 'root'
})
export class AppConfigService extends ApiService {
  // tslint:disable-next-line:variable-name
  private _config: AppConfigModel;

  load(): Promise<AppConfigModel> {
    return this.http
      .get<AppConfigModel>(this.getApiUrl('/settings'))
      .toPromise()
      .then((data: AppConfigModel) => {
        APP_CONFIG.data = data;
        return data;
      });
  }

  getCurrentLang() {
    return APP_CONFIG.data.defaultLocale;
  }

  get config() {
    return this._config;
  }
}
