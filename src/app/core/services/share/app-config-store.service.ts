import { AppConfigModel, AppSpecialAbs } from '../../models/app-config.model';
import { environment } from 'src/environments/environment.prod';

export let APP_CONFIG = {
  data: new AppConfigModel(),
  specialAbs: new AppSpecialAbs()
};

export function setDefaultLang(locale: string) {
  APP_CONFIG.data.defaultLocale = locale;
  localStorage.setItem(environment.langConst, locale);
}
