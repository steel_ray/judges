import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe, registerLocaleData } from '@angular/common';
import { APP_CONFIG } from '../../services/share/app-config-store.service';
import localeRU from '@angular/common/locales/ru';
import localeUZ from '@angular/common/locales/uz';
import localeUZCyrl from '@angular/common/locales/uz-Cyrl';

registerLocaleData(localeRU);
registerLocaleData(localeUZ);
registerLocaleData(localeUZCyrl);

@Pipe({
  name: 'datePipe'
})
export class DatePipePipe implements PipeTransform {
  transform(value: number, ...args: string[]): any {
    return new DatePipe(APP_CONFIG.data.defaultLocale).transform(
      value,
      args[0]
    );
  }
}
