import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'menuUrl'
})
export class MenuUrlPipe implements PipeTransform {
  transform(url: string, view: string) {
    const action = view === 'library' ? '/library' : '/category';
    return url.charAt(0) === '/' ? url : [action, url];
  }
}
