import { NgModule } from '@angular/core';
import { MenuUrlPipe } from './menu-url.pipe';

@NgModule({
  declarations: [MenuUrlPipe],
  exports: [MenuUrlPipe]
})
export class MenuUrlModule {}
