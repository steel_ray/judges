import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { APP_CONFIG } from '../services/share/app-config-store.service';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class LangInterceptor implements HttpInterceptor {
  constructor(private translate: TranslateService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const lang = APP_CONFIG.data.defaultLocale;
    // this.translate.onLangChange.subscribe(res => {
    //   lang = res.lang;
    // });
    request = request.clone({
      setParams: {
        lang
      }
    });
    return next.handle(request);
  }
}
