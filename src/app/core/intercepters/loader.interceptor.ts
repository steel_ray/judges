import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LoaderService } from '../services/api/loader.service';
@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
  counter = 0;
  constructor(public loaderService: LoaderService) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.loaderService.show();
    // return next.handle(req);
    this.counter++;
    return next.handle(req).pipe(
      finalize(() => {
        this.counter--;
        // console.log(this.counter);
        if (this.counter === 0) {
          this.loaderService.hide();
        }
      })
    );
  }
}
