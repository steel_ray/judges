import { Directive, OnInit, ElementRef } from '@angular/core';
import { APP_CONFIG } from '../../services/share/app-config-store.service';
import Speech from 'speak-tts';

declare var $: any;

@Directive({
  selector: '[lnVoice]'
})
export class VoiceDirective implements OnInit {
  constructor(private el: ElementRef) {}

  ngOnInit() {
    const appLang = APP_CONFIG.data.defaultLocale;
    const elHover = $(this.el.nativeElement);
    function _init() {
      const speech = new Speech();
      speech
        .init({
          volume: 0.2,
          lang: appLang,
          rate: 1,
          pitch: 1
          // voice: 'Google UK English Female',
          // tslint:disable-next-line:comment-format
          //'splitSentences': false,
          // listeners: {
          //   onvoiceschanged: voices => {
          //     // console.log('Voices changed', voices);
          //   }
          // }
        })
        .then(data => {
          // console.log('Speech is ready', data);
          _prepareSpeakButton(speech);
        })
        .catch(e => {
          console.error('An error occured while initializing : ', e);
        });
    }

    function _prepareSpeakButton(speech) {
      // const textToSpeech = 'Wow!';

      $(elHover).on('mouseenter', function() {
        let hovered = 0;
        const thisEl = $(this);
        const textToSpeech = $(this).text();
        const speechActivated = APP_CONFIG.specialAbs.textSpeech;
        setTimeout(() => {
          if (speechActivated && hovered === 0 && textToSpeech) {
            const language = appLang;
            let voice = 'Russian';
            if (language === 'en') {
              voice = 'English_(America)';
            }
            if (language) {
              speech.setLanguage(language);
            }
            if (voice) {
              speech.setVoice(voice);
            }
            speech
              .speak({
                text: textToSpeech,
                queue: false,
                listeners: {
                  onstart: () => {
                    // console.log('Start utterance');
                  },
                  onend: () => {
                    // console.log('End utterance');
                  },
                  onresume: () => {
                    // console.log('Resume utterance');
                  },
                  onboundary: event => {
                    // console.log(
                    //   event.name +
                    //     ' boundary reached after ' +
                    //     event.elapsedTime +
                    //     ' milliseconds.'
                    // );
                  }
                }
              })
              .then(data => {
                // console.log('Success !', data);
              })
              .catch(e => {
                // console.error('An error occurred :', e);
              });
          }
          hovered = 1;
        }, 500);
      });

      setTimeout(() => {}, 2000);
    }

    _init();
  }
}
