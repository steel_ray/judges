import { NgModule } from '@angular/core';
import { VoiceDirective } from './voice.directive';

@NgModule({
  declarations: [VoiceDirective],
  exports: [VoiceDirective]
})
export class VoiceModule {}
