import { Directive, ElementRef, OnInit, AfterViewInit } from '@angular/core';

declare var $: any;

@Directive({
  selector: '[lnFixedMenu]'
})
export class FixedMenuDirective implements AfterViewInit {
  ngAfterViewInit(): void {
    try {
      $(window).ready(() => {
        const posNav = $('.header__menu').offset().top;
        const menuDesktop = $('.header');
        const mainNav = $('.header__menu');
        let lastScrollTop = 0;
        let st = 0;
        $(window).on('scroll', () => {
          fixedHeader();
        });

        $(window).on('resize', () => {
          fixedHeader();
        });

        $(window).on('load', () => {
          fixedHeader();
        });

        const fixedHeader = () => {
          st = $(window).scrollTop();

          if (st > posNav + mainNav.outerHeight()) {
            $(menuDesktop).addClass('header-fixed');
          } else if (st <= posNav) {
            $(menuDesktop).removeClass('header-fixed');
          }

          if (st > lastScrollTop) {
            $(menuDesktop).removeClass('header-show');
          } else {
            $(menuDesktop).addClass('header-show');
          }

          lastScrollTop = st;
        };
      });
    } catch (err) {
      console.log(err);
    }
  }
}
