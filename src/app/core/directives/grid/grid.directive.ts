import { Directive, ElementRef, OnInit, AfterViewInit } from '@angular/core';
declare var $: any;
@Directive({
  selector: '[lnGrid]'
})
export class GridDirective implements AfterViewInit {
  constructor(private el: ElementRef) {}

  ngAfterViewInit(): void {
    $(document).ready(() => {
      $(this.el.nativeElement).tightGrid({
        //  CSS selector for grid items
        itemSelector: '.js-item',

        // width of the grid item
        // null = takes the first item width
        columnWidth: null,

        // re-init the grid layout on resize event
        // good for responsive design
        resize: true
      });
    });
  }
}
