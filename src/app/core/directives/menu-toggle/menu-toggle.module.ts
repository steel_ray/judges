import { NgModule } from '@angular/core';
import { MenuToggleDirective } from './menu-toggle.directive';

@NgModule({
  declarations: [MenuToggleDirective],
  exports: [MenuToggleDirective]
})
export class MenuToggleModule {}
