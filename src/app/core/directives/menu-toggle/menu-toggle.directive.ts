import { Directive, ElementRef, OnInit } from '@angular/core';
declare var $: any;
@Directive({
  selector: '[lnMenuToggle]'
})
export class MenuToggleDirective implements OnInit {
  target: string;
  constructor(private el: ElementRef) {}

  ngOnInit(): void {
    this.el.nativeElement.addEventListener('click', this.toggleMenu);
  }

  toggleMenu(e) {
    $(document).ready(() => {
      const targetName = $(this).data('target');
      const close = $(this).data('close');
      if (targetName) {
        const target = $(targetName);
        if (!close) {
          // console.log('ok');
          $('body').toggleClass('overflowed');
          if (!target.is('.active')) {
            target.fadeIn('normal', function() {
              $(this).addClass('active');
            });
          } else {
            target.fadeOut('normal', function() {
              $(this).removeClass('active');
            });
          }
        } else {
          target.fadeOut('normal', () => {
            target.removeClass('active');
            $('body').removeClass('overflowed');
          });
        }
      }
    });
  }
}
