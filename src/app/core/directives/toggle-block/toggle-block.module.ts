import { NgModule } from '@angular/core';
import { ToggleBlockDirective } from './toggle-block.directive';

@NgModule({
  declarations: [ToggleBlockDirective],
  exports: [ToggleBlockDirective]
})
export class ToggleBlockModule {}
