import { Directive, ElementRef, OnInit } from '@angular/core';
declare var $: any;
@Directive({
  selector: '[lnToggleBlock]'
})
export class ToggleBlockDirective implements OnInit {
  constructor(private el: ElementRef) {}

  ngOnInit(): void {
    try {
      $(document).ready(() => {
        $(this.el.nativeElement).click(function(e) {
          e.preventDefault();
          const target = $(this).data('target');
          $(this).toggleClass('active');
          $(target).slideToggle('fast');
        });

        $(window).click(e => {
          const target = $(this.el.nativeElement).data('target');
          if (
            $(e.target).closest(
              target + ',' + $(this.el.nativeElement).data('self')
            ).length === 0
          ) {
            $(target).slideUp('fast');
            $(this.el.nativeElement).removeClass('active');
          }
        });
      });
    } catch (err) {
      console.log(err);
    }
  }
}
