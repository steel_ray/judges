import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SafeModule } from '../pipes/safe/safe.module';
import { DatePipeModule } from '../pipes/date-pipe/date-pipe.module';
import { VoiceModule } from '../directives/voice/voice.module';

@NgModule({
  exports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    SafeModule,
    DatePipeModule,
    VoiceModule
  ]
})
export class SharedModule {}
