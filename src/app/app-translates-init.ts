import { TranslateService } from '@ngx-translate/core';
import { APP_CONFIG } from './core/services/share/app-config-store.service';

export class AppTranslateInit {
  constructor(private translate: TranslateService) {}
  init() {
    // console.log();
    const locales = APP_CONFIG.data.locales.map(l => l.locale);
    const defaultLocale = APP_CONFIG.data.defaultLocale;
    // console.log(locales, defaultLocale);
    this.translate.addLangs(locales);
    this.translate.setDefaultLang(defaultLocale);
    // const browserLang = this.translate.getBrowserLang();
    localStorage.setItem('LN_LOCALE', defaultLocale);
    this.translate.use(
      // locales.find(l => l === browserLang) ? browserLang : defaultLocale
      defaultLocale
    );
  }
}
