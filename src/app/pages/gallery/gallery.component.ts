import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuInt } from 'src/app/core/interfaces/menu.int';
import { MenuService } from 'src/app/core/services/api/menu.service';
import { searchMenuItems } from 'src/app/core/utility/helpers';
import { AlbumInt } from 'src/app/core/interfaces/album.int';
import { AlbumService } from 'src/app/core/services/api/album.service';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ln-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit, OnDestroy {
  parent: MenuInt = null;
  currentMenu: MenuInt = null;
  subItems: MenuInt[] = [];
  slug = '/gallery';
  albums$: Observable<AlbumInt[]>;
  unsub$ = new Subject<void>();
  constructor(
    private menuService: MenuService,
    private albumService: AlbumService,
    private transalte: TranslateService
  ) {}

  ngOnInit() {
    this.loadData();
    this.transalte.onLangChange
      .pipe(takeUntil(this.unsub$))
      .subscribe(res => setTimeout(_ => this.loadData()));
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  loadData() {
    this.menuService.getItems().subscribe(res => {
      const { current, parent, subItems } = searchMenuItems(res, this.slug);
      this.currentMenu = current;
      this.parent = parent;
      this.subItems = subItems;
      this.albums$ = this.albumService.fetchAll({
        category_slug: 'gallery'
      });
    });
  }
}
