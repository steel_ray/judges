import { NgModule } from '@angular/core';

import { GalleryRoutingModule } from './gallery-routing.module';
import { GalleryComponent } from './gallery.component';
import { LayoutModule } from 'src/app/ui/layout/layout.module';
import { PageHeaderModule } from 'src/app/ui/page-header/page-header.module';
import { SidebarModule } from 'src/app/ui/sidebar/sidebar.module';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [GalleryComponent],
  imports: [
    SharedModule,
    GalleryRoutingModule,
    LayoutModule,
    PageHeaderModule,
    SidebarModule
  ]
})
export class GalleryModule {}
