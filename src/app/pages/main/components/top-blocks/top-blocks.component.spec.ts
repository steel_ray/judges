import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopBlocksComponent } from './top-blocks.component';

describe('TopBlocksComponent', () => {
  let component: TopBlocksComponent;
  let fixture: ComponentFixture<TopBlocksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopBlocksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopBlocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
