import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostService } from 'src/app/core/services/api/post.service';
import { Observable, Subscription, Subject } from 'rxjs';
import { PostsInt } from 'src/app/core/interfaces/posts.int';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ln-top-blocks',
  templateUrl: './top-blocks.component.html',
  styleUrls: ['./top-blocks.component.scss']
})
export class TopBlocksComponent implements OnInit, OnDestroy {
  topNews$: Observable<PostsInt[]>;
  limit = 4;
  unsub$ = new Subject<void>();
  constructor(
    private postService: PostService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.loadData();
    this.translate.onLangChange
      .pipe(takeUntil(this.unsub$))
      .subscribe(res => setTimeout(_ => this.loadData()));
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  loadData() {
    this.topNews$ = this.postService.fetchAll({
      category_slug: 'novosti',
      order: 'top',
      limit: this.limit
    });
  }
}
