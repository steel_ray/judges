import { NgModule } from '@angular/core';
import { TopBlocksComponent } from './top-blocks.component';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [TopBlocksComponent],
  imports: [SharedModule],
  exports: [TopBlocksComponent]
})
export class TopBlocksModule {}
