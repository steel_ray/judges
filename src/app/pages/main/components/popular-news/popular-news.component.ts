import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PostsInt } from 'src/app/core/interfaces/posts.int';
import { takeUntil } from 'rxjs/operators';
import { PostService } from 'src/app/core/services/api/post.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ln-popular-news',
  templateUrl: './popular-news.component.html',
  styleUrls: ['./popular-news.component.scss']
})
export class PopularNewsComponent implements OnInit, OnDestroy {
  posts$: Observable<PostsInt[]>;
  limit = 4;
  unsub$ = new Subject<void>();
  constructor(
    private postService: PostService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.loadData();
    this.translate.onLangChange
      .pipe(takeUntil(this.unsub$))
      .subscribe(res => setTimeout(_ => this.loadData()));
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  loadData() {
    this.posts$ = this.postService.fetchAll({
      category_slug: 'novosti',
      order: 'popular',
      limit: this.limit
    });
  }
}
