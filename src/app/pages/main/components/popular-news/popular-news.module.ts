import { NgModule } from '@angular/core';
import { PopularNewsComponent } from './popular-news.component';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [PopularNewsComponent],
  imports: [SharedModule],
  exports: [PopularNewsComponent]
})
export class PopularNewsModule {}
