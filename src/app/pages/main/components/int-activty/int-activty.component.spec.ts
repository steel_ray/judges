import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntActivtyComponent } from './int-activty.component';

describe('IntActivtyComponent', () => {
  let component: IntActivtyComponent;
  let fixture: ComponentFixture<IntActivtyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntActivtyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntActivtyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
