import { NgModule } from '@angular/core';
import { IntActivtyComponent } from './int-activty.component';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [IntActivtyComponent],
  imports: [SharedModule],
  exports: [IntActivtyComponent]
})
export class IntActivtyModule {}
