import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostsInt } from 'src/app/core/interfaces/posts.int';
import { PostService } from 'src/app/core/services/api/post.service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ln-main-tabs',
  templateUrl: './main-tabs.component.html',
  styleUrls: ['./main-tabs.component.scss']
})
export class MainTabsComponent implements OnInit, OnDestroy {
  activeTab = 0;
  limit = 4;
  posts: PostsInt[];
  sub: Subscription = new Subscription();
  constructor(
    private postService: PostService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.loadData();
    this.sub.add(
      this.translate.onLangChange.subscribe(res =>
        setTimeout(_ => this.loadData())
      )
    );
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  loadData(): void {
    this.sub.add(
      this.postService
        .fetchAll({
          category_slug: 'tabs-articles',
          limit: this.limit
        })
        .subscribe(res => {
          this.posts = res;
          this.setTabContHeight();
        })
    );
  }

  onTabChange(id: string): void {
    try {
      // console.log(el);
      const el = document.getElementById(id);
      const tabNum = +el.dataset.panel;
      this.activeTab = tabNum;
      this.setTabContHeight();
    } catch (err) {
      console.log(err);
    }
  }

  setTabContHeight() {
    try {
      setTimeout(_ => {
        const activeTabDOM: any = document.getElementById(
          `panel${this.activeTab}`
        );
        const tabsCont: any = document.getElementsByClassName(
          'main-tabs__panels-inner'
        )[0];
        tabsCont.style.height = `${activeTabDOM.offsetHeight}px`;
      });
    } catch (err) {
      console.log(err);
    }
  }
}
