import { NgModule } from '@angular/core';
import { MainTabsComponent } from './main-tabs.component';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [MainTabsComponent],
  imports: [SharedModule],
  exports: [MainTabsComponent]
})
export class MainTabsModule {}
