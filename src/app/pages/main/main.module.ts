import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { LayoutModule } from 'src/app/ui/layout/layout.module';
import { TopBlocksModule } from './components/top-blocks/top-blocks.module';
import { MainTabsModule } from './components/main-tabs/main-tabs.module';
import { PopularNewsModule } from './components/popular-news/popular-news.module';
import { IntActivtyModule } from './components/int-activty/int-activty.module';

@NgModule({
  declarations: [MainComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    LayoutModule,
    TopBlocksModule,
    MainTabsModule,
    PopularNewsModule,
    IntActivtyModule
  ]
})
export class MainModule {}
