import { NgModule } from '@angular/core';

import { SitemapRoutingModule } from './sitemap-routing.module';
import { SitemapComponent } from './sitemap.component';
import { SharedModule } from 'src/app/core/shared/shared.module';
import { LayoutModule } from 'src/app/ui/layout/layout.module';
import { PageHeaderModule } from 'src/app/ui/page-header/page-header.module';
import { SidebarModule } from 'src/app/ui/sidebar/sidebar.module';
import { MenuUrlModule } from 'src/app/core/pipes/menu-url/menu-url.module';

@NgModule({
  declarations: [SitemapComponent],
  imports: [
    SharedModule,
    SitemapRoutingModule,
    LayoutModule,
    PageHeaderModule,
    SidebarModule,
    MenuUrlModule
  ]
})
export class SitemapModule {}
