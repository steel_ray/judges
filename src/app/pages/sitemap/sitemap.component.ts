import { Component, OnInit } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { MenuInt } from 'src/app/core/interfaces/menu.int';
import { MenuService } from 'src/app/core/services/api/menu.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'ln-sitemap',
  templateUrl: './sitemap.component.html',
  styleUrls: ['./sitemap.component.scss']
})
export class SitemapComponent implements OnInit {
  menuItems$: Observable<MenuInt[]>;
  constructor(private menuService: MenuService) {}

  ngOnInit() {
    this.menuItems$ = combineLatest(
      this.menuService.getItems(),
      this.menuService.getItems('top-menu')
    ).pipe(map(res => res[0].concat(res[1])));
  }
}
