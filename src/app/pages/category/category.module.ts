import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule } from '@angular/material/paginator';

import { CategoryRoutingModule } from './category-routing.module';
import { CategoryComponent } from './category.component';
import { LayoutModule } from 'src/app/ui/layout/layout.module';
import { SidebarModule } from 'src/app/ui/sidebar/sidebar.module';
import { PageHeaderModule } from 'src/app/ui/page-header/page-header.module';
import { CalendarModule } from './components/calendar/calendar.module';
import { DocListModule } from './components/doc-list/doc-list.module';
import { MassMediaModule } from './components/mass-media/mass-media.module';
import { AdsModule } from './components/ads/ads.module';
import { NewsModule } from './components/news/news.module';
import { CategoryDefaultViewModule } from './components/category-default-view/category-default-view.module';

@NgModule({
  declarations: [CategoryComponent],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    LayoutModule,
    SidebarModule,
    PageHeaderModule,
    CalendarModule,
    DocListModule,
    MassMediaModule,
    AdsModule,
    NewsModule,
    CategoryDefaultViewModule,
    MatPaginatorModule
  ]
})
export class CategoryModule {}
