import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuService } from 'src/app/core/services/api/menu.service';
import { combineLatest, Subscription, Observable, Subject } from 'rxjs';
import { MenuInt } from 'src/app/core/interfaces/menu.int';
import { TranslateService } from '@ngx-translate/core';
import { PostsInt } from 'src/app/core/interfaces/posts.int';
import { PostService } from 'src/app/core/services/api/post.service';
import { takeUntil, map, tap } from 'rxjs/operators';
import { APP_CONFIG } from 'src/app/core/services/share/app-config-store.service';
import { searchMenuItems } from 'src/app/core/utility/helpers';
@Component({
  selector: 'ln-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnDestroy {
  slug: string;
  parentCat: MenuInt;
  currCat: MenuInt;
  sub: Subscription;
  subItems: MenuInt[] = [];
  posts: PostsInt[];
  unsub$ = new Subject<void>();
  limit = APP_CONFIG.data.pageSize;
  page = 0;
  totalCount: number;
  limits = [10, 25, 50, 100];
  datePickerDate = null;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private menuService: MenuService,
    private translate: TranslateService,
    private postService: PostService
  ) {}

  ngOnInit() {
    this.page = +this.activatedRoute.snapshot.queryParamMap.get('page');
    this.loadData();
    this.translate.onLangChange
      .pipe(takeUntil(this.unsub$))
      .subscribe(e => setTimeout(_ => this.loadData()));
  }

  loadData() {
    const combined = combineLatest(
      this.activatedRoute.params,
      this.menuService.getItems(),
      this.menuService.getItems('top-menu')
    );
    this.sub = combined.subscribe((res: any) => {
      this.currCat = null;
      this.parentCat = null;
      this.subItems = [];
      this.slug = res[0].slug;
      const menuItems = res[1].concat(res[2]);
      const { current, parent, subItems } = searchMenuItems(
        menuItems,
        this.slug
      );
      this.currCat = current;
      this.parentCat = parent;
      this.subItems = subItems;
      if (!this.currCat) {
        this.router.navigateByUrl('**', { skipLocationChange: true }); // to 404
        return;
      }

      const params: any = {
        category_slug: this.slug,
        limit: this.limit,
        page: this.page
      };
      if (this.currCat.view === 'calendar') {
        params.published_at = this.datePickerDate
          ? this.datePickerDate
          : Math.floor(Date.now()) / 1000;
        this.postService
          .fetchAll(params, false, 'response')
          .subscribe((_: any) => {
            this.setPaginatorBody(_);
          });
      } else {
        this.postService.fetchAll(params, true, 'response').subscribe(_ => {
          this.setPaginatorBody(_);
        });
      }
    });
  }

  setPaginatorBody(res) {
    this.posts = res.body;
    const headers = res.headers;
    this.totalCount = headers.get('X-Pagination-Total-Count');
  }

  onDatePickerChange(date: number) {
    this.datePickerDate = date;
    this.loadData();
  }

  goToPage(e) {
    this.page = e.pageIndex;
    this.loadData();
    this.router.navigate(['/category', this.slug], {
      queryParams: { page: this.page }
    });
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    this.unsub$.next();
    this.unsub$.complete();
  }
}
