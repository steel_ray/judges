import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { PostsInt } from 'src/app/core/interfaces/posts.int';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE
} from '@angular/material/core';
import { APP_CONFIG } from 'src/app/core/services/share/app-config-store.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ln-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: APP_CONFIG.data.defaultLocale },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class CalendarComponent implements OnInit {
  selectedDate = Date.now();
  @Input() posts: PostsInt[];
  datePickerInit = true;
  @Output() dateChaged = new EventEmitter<number>();

  selectedChange(e) {
    // console.log(e._d.getTime());
    this.selectedDate = e._d;
    const date = e._d.getTime() / 1000;
    this.dateChaged.emit(date);
  }
  userSelection() {}

  constructor(
    private translate: TranslateService,
    private adapter: DateAdapter<any>
  ) {}

  ngOnInit() {
    this.translate.onLangChange.subscribe(res => {
      this.datePickerInit = false;
      this.adapter.setLocale(res.lang);
      setTimeout(_ => (this.datePickerInit = true));
    });
  }
}
