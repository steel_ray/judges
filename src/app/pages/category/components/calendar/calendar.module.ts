import { NgModule } from '@angular/core';
import { CalendarComponent } from './calendar.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  exports: [CalendarComponent],
  declarations: [CalendarComponent],
  imports: [SharedModule, MatDatepickerModule, MatNativeDateModule]
  // providers: [{ provide: MAT_DATE_LOCALE, useValue:  }]
})
export class CalendarModule {}
