import { Component, Input } from '@angular/core';
import { PostsInt } from 'src/app/core/interfaces/posts.int';

@Component({
  selector: 'ln-category-default-view',
  templateUrl: './category-default-view.component.html',
  styleUrls: ['./category-default-view.component.scss']
})
export class CategoryDefaultViewComponent {
  @Input() posts: PostsInt[] = [];
}
