import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryDefaultViewComponent } from './category-default-view.component';

describe('CategoryDefaultViewComponent', () => {
  let component: CategoryDefaultViewComponent;
  let fixture: ComponentFixture<CategoryDefaultViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryDefaultViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryDefaultViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
