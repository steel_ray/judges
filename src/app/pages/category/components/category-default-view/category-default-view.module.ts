import { NgModule } from '@angular/core';
import { CategoryDefaultViewComponent } from './category-default-view.component';
import { SharedModule } from 'src/app/core/shared/shared.module';
import { DefaultArticlesListModule } from 'src/app/ui/default-articles-list/default-articles-list.module';

@NgModule({
  declarations: [CategoryDefaultViewComponent],
  imports: [SharedModule, DefaultArticlesListModule],
  exports: [CategoryDefaultViewComponent]
})
export class CategoryDefaultViewModule {}
