import { NgModule } from '@angular/core';
import { AdsComponent } from './ads.component';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [AdsComponent],
  imports: [SharedModule],
  exports: [AdsComponent]
})
export class AdsModule {}
