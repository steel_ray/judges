import { Component, Input } from '@angular/core';
import { PostsInt } from 'src/app/core/interfaces/posts.int';

@Component({
  selector: 'ln-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.scss']
})
export class AdsComponent {
  @Input() posts: PostsInt[] = [];
}
