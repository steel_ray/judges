import { Component, Input } from '@angular/core';
import { PostsInt } from 'src/app/core/interfaces/posts.int';

@Component({
  selector: 'ln-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent {
  viewType = 'list'; // grid
  @Input() posts: PostsInt[] = [];

  onViewChange(type: 'grid' | 'list'): void {
    this.viewType = type;
  }
}
