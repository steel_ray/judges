import { NgModule } from '@angular/core';
import { MassMediaComponent } from './mass-media.component';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [MassMediaComponent],
  imports: [SharedModule],
  exports: [MassMediaComponent]
})
export class MassMediaModule {}
