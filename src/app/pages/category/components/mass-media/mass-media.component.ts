import { Component, Input } from '@angular/core';
import { PostsInt } from 'src/app/core/interfaces/posts.int';

@Component({
  selector: 'ln-mass-media',
  templateUrl: './mass-media.component.html',
  styleUrls: ['./mass-media.component.scss']
})
export class MassMediaComponent {
  @Input() posts: PostsInt[] = [];
}
