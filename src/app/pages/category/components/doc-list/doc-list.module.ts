import { NgModule } from '@angular/core';
import { DocListComponent } from './doc-list.component';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [DocListComponent],
  imports: [SharedModule],
  exports: [DocListComponent]
})
export class DocListModule {}
