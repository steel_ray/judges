import { Component, OnInit, Input } from '@angular/core';
import { PostsInt } from 'src/app/core/interfaces/posts.int';

@Component({
  selector: 'ln-doc-list',
  templateUrl: './doc-list.component.html',
  styleUrls: ['./doc-list.component.scss']
})
export class DocListComponent {
  @Input() posts: PostsInt[] = [];
}
