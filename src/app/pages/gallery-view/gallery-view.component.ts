import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlbumService } from 'src/app/core/services/api/album.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject, Subscription, combineLatest } from 'rxjs';
import { AlbumInt } from 'src/app/core/interfaces/album.int';
import { MenuService } from 'src/app/core/services/api/menu.service';
import { MenuInt } from 'src/app/core/interfaces/menu.int';
import { searchMenuItems } from 'src/app/core/utility/helpers';

@Component({
  selector: 'ln-gallery-view',
  templateUrl: './gallery-view.component.html',
  styleUrls: ['./gallery-view.component.scss']
})
export class GalleryViewComponent implements OnInit, OnDestroy {
  album$: Observable<AlbumInt>;
  albums: AlbumInt[];
  sub: Subscription = new Subscription();
  slug: string;
  parent: MenuInt = null;
  currentMenu: MenuInt = null;
  subItems: MenuInt[] = [];
  constructor(
    private albumService: AlbumService,
    private translate: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private menuService: MenuService
  ) {}

  ngOnInit() {
    this.loadData();
    this.sub.add(
      this.translate.onLangChange.subscribe(res =>
        setTimeout(_ => this.loadData())
      )
    );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  loadData() {
    const combined = combineLatest(
      this.activatedRoute.params,
      this.albumService.fetchAll('gallery'),
      this.menuService.getItems()
    );
    this.sub.add(
      combined.subscribe(res => {
        this.slug = res[0].slug;
        this.albums = res[1];

        const check = this.albums.findIndex(a => a.slug === this.slug);

        if (check < 0) {
          this.router.navigate(['**'], { skipLocationChange: true });
          return;
        }

        const { current, parent, subItems } = searchMenuItems(
          res[2],
          '/gallery'
        );
        this.currentMenu = current;
        this.parent = parent;
        this.subItems = subItems;
        this.album$ = this.albumService.fetchOne(this.slug);
      })
    );
  }

  onSelectionChange(slug: string) {
    this.router.navigate(['/gallery', slug]);
  }
}
