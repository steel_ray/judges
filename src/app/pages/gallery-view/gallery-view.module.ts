import { NgModule } from '@angular/core';

import { MatSelectModule } from '@angular/material/select';
import { MatGridListModule } from '@angular/material/grid-list';

import { GalleryViewRoutingModule } from './gallery-view-routing.module';
import { GalleryViewComponent } from './gallery-view.component';
import { LayoutModule } from 'src/app/ui/layout/layout.module';
import { PageHeaderModule } from 'src/app/ui/page-header/page-header.module';
import { SidebarModule } from 'src/app/ui/sidebar/sidebar.module';

import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [GalleryViewComponent],
  imports: [
    SharedModule,
    GalleryViewRoutingModule,
    MatGridListModule,
    LayoutModule,
    PageHeaderModule,
    SidebarModule,
    MatSelectModule
  ]
})
export class GalleryViewModule {}
