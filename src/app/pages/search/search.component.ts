import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PostService } from 'src/app/core/services/api/post.service';
import { Observable, Subject } from 'rxjs';
import { PostsInt } from 'src/app/core/interfaces/posts.int';
import { MenuService } from 'src/app/core/services/api/menu.service';
import { MenuInt } from 'src/app/core/interfaces/menu.int';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ln-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  q: string;
  posts$: Observable<PostsInt[]>;
  subItems$: Observable<MenuInt[]>;
  unsub$ = new Subject<void>();
  constructor(
    private activatedRoute: ActivatedRoute,
    private postService: PostService,
    private router: Router,
    private menuService: MenuService,
    private translate: TranslateService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.loadData();
    this.translate.onLangChange
      .pipe(takeUntil(this.unsub$))
      .subscribe(res => setTimeout(_ => this.loadData()));
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  loadData() {
    this.q = this.activatedRoute.snapshot.queryParamMap.get('q');
    this.posts$ = this.postService.fetchAll({ q: this.q }, false);
    // this.posts$.subscribe(res => console.log(res));
    this.subItems$ = this.menuService.getItems();
  }
}
