import { NgModule } from '@angular/core';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { SharedModule } from 'src/app/core/shared/shared.module';
import { LayoutModule } from 'src/app/ui/layout/layout.module';
import { PageHeaderModule } from 'src/app/ui/page-header/page-header.module';
import { SidebarModule } from 'src/app/ui/sidebar/sidebar.module';
import { DefaultArticlesListModule } from 'src/app/ui/default-articles-list/default-articles-list.module';

@NgModule({
  declarations: [SearchComponent],
  imports: [
    SharedModule,
    SearchRoutingModule,
    LayoutModule,
    SidebarModule,
    PageHeaderModule,
    DefaultArticlesListModule
  ]
})
export class SearchModule {}
