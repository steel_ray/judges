import { NgModule } from '@angular/core';

import { PostRoutingModule } from './post-routing.module';
import { PostComponent } from './post.component';
import { SharedModule } from 'src/app/core/shared/shared.module';
import { LayoutModule } from 'src/app/ui/layout/layout.module';
import { PageHeaderModule } from 'src/app/ui/page-header/page-header.module';
import { SidebarModule } from 'src/app/ui/sidebar/sidebar.module';
import { SafeModule } from 'src/app/core/pipes/safe/safe.module';

@NgModule({
  declarations: [PostComponent],
  imports: [
    SharedModule,
    LayoutModule,
    PageHeaderModule,
    PostRoutingModule,
    SidebarModule,
    SafeModule
  ]
})
export class PostModule {}
