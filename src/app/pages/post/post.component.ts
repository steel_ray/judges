import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject, Subscription, combineLatest } from 'rxjs';
import { PostInt } from 'src/app/core/interfaces/post.int';
import { MenuInt } from 'src/app/core/interfaces/menu.int';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuService } from 'src/app/core/services/api/menu.service';
import { TranslateService } from '@ngx-translate/core';
import { PostService } from 'src/app/core/services/api/post.service';
import { takeUntil } from 'rxjs/operators';
import { searchMenuItems } from 'src/app/core/utility/helpers';

@Component({
  selector: 'ln-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit, OnDestroy {
  slug: string;
  parentCat: MenuInt;
  currCat: MenuInt;
  subItems: MenuInt[] = [];
  post: PostInt;
  unsub$ = new Subject<void>();
  sub: Subscription = new Subscription();
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private menuService: MenuService,
    private translate: TranslateService,
    private postService: PostService
  ) {}

  ngOnInit() {
    this.loadData();
    this.translate.onLangChange
      .pipe(takeUntil(this.unsub$))
      .subscribe(e => setTimeout(_ => this.loadData()));
  }

  loadData() {
    this.sub.add(
      this.activatedRoute.params.subscribe(params => {
        this.slug = params.slug;
        const combined = combineLatest(
          this.postService.fetchOne(this.slug),
          this.menuService.getItems(),
          this.menuService.getItems('top-menu')
        );
        this.sub.add(
          combined.subscribe((res: any) => {
            this.post = res[0];
            // console.log(this.post);
            if (this.post.status && this.post.status === 404) {
              this.router.navigate(['**'], { skipLocationChange: true });
              return;
            }

            this.currCat = null;
            this.parentCat = null;
            this.subItems = [];
            const menuItems = res[1].concat(res[2]);
            const { current, parent, subItems } = searchMenuItems(
              menuItems,
              this.post.category.slug
            );
            this.currCat = current;
            this.parentCat = parent;
            this.subItems = subItems;
          })
        );
      })
    );
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.unsub$.next();
    this.unsub$.complete();
  }
}
