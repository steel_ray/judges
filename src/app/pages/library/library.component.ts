import { Component, OnInit, OnDestroy } from '@angular/core';
import { combineLatest, Subject, Subscription } from 'rxjs';
import { PostsInt } from 'src/app/core/interfaces/posts.int';
import { PostService } from 'src/app/core/services/api/post.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuService } from 'src/app/core/services/api/menu.service';
import { MenuInt } from 'src/app/core/interfaces/menu.int';
import { takeUntil } from 'rxjs/operators';
import { searchMenuItems } from 'src/app/core/utility/helpers';

@Component({
  selector: 'ln-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss']
})
export class LibraryComponent implements OnInit, OnDestroy {
  slug: string;
  parentCat: MenuInt;
  currCat: MenuInt;
  sub: Subscription;
  subItems: MenuInt[] = [];
  posts: PostsInt[];
  unsub$ = new Subject<void>();
  limit = 2;
  page = 0;
  totalCount: number;
  limits = [10, 25, 50, 100];
  currentTab = 0;
  tabUrlArr = ['authors', 'files', 'keywords', 'literature'];
  constructor(
    private postService: PostService,
    private translate: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private menuService: MenuService
  ) {}

  ngOnInit() {
    this.page = +this.activatedRoute.snapshot.queryParamMap.get('page');
    this.loadData();
    this.translate.onLangChange
      .pipe(takeUntil(this.unsub$))
      .subscribe(e => setTimeout(_ => this.loadData()));
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    this.unsub$.next();
    this.unsub$.complete();
  }

  onTabChange(e) {
    const queryParams: any = {};
    if (this.page) {
      queryParams.page = this.page;
    }
    // console.log(queryParams);
    this.router.navigate(['/library', this.slug, this.tabUrlArr[e.index]], {
      queryParams
    });
  }

  loadData() {
    const combined = combineLatest(
      this.activatedRoute.params,
      this.menuService.getItems(),
      this.menuService.getItems('top-menu')
    );
    this.sub = combined.subscribe((res: any) => {
      this.currCat = null;
      this.parentCat = null;
      this.subItems = [];

      this.slug = res[0].slug;
      this.currentTab = res[0].tab
        ? this.tabUrlArr.findIndex(t => t === res[0].tab)
        : 0;

      const menuItems = res[1].concat(res[2]);
      const { current, parent, subItems } = searchMenuItems(
        menuItems,
        this.slug
      );
      this.currCat = current;
      this.parentCat = parent;
      this.subItems = subItems;

      if (!this.currCat || this.currentTab === -1) {
        this.router.navigateByUrl('**', { skipLocationChange: true }); // to 404
        return;
      }
      const params = {
        category_slug: this.slug,
        page: this.page,
        limit: this.limit,
        expand: 'author_name,tags,content'
      };
      this.postService.fetchAll(params, true, 'response').subscribe(_ => {
        this.setPaginatorBody(_);
      });
    });
  }

  goToPage(e) {
    this.page = e.pageIndex;
    this.loadData();
    this.router.navigate(
      ['/library', this.slug, this.tabUrlArr[this.currentTab]],
      {
        queryParams: { page: this.page }
      }
    );
  }

  setPaginatorBody(res) {
    this.posts = res.body;
    const headers = res.headers;
    this.totalCount = headers.get('X-Pagination-Total-Count');
  }
}
