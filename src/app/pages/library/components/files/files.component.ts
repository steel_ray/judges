import { Component, OnInit, Input } from '@angular/core';
import { PostsInt } from 'src/app/core/interfaces/posts.int';

@Component({
  selector: 'ln-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss']
})
export class FilesComponent implements OnInit {
  @Input() posts: PostsInt[];

  constructor() {}

  ngOnInit() {
    this.posts = this.posts.filter(p => p.document);
  }
}
