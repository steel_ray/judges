import { Component, OnInit, Input } from '@angular/core';
import { PostsInt } from 'src/app/core/interfaces/posts.int';
import { onlyUniqueObject } from 'src/app/core/utility/helpers';

@Component({
  selector: 'ln-keywords',
  templateUrl: './keywords.component.html',
  styleUrls: ['./keywords.component.scss']
})
export class KeywordsComponent implements OnInit {
  @Input() posts: PostsInt[];
  keywords: any = [];
  constructor() {}

  ngOnInit() {
    const tags = this.posts.map(m => m.tags);
    const merged = [].concat.apply([], tags);
    this.keywords = onlyUniqueObject(merged, 'slug');
  }
}
