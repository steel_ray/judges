import { Component, OnInit, Input } from '@angular/core';
import { PostsInt } from 'src/app/core/interfaces/posts.int';

@Component({
  selector: 'ln-literature',
  templateUrl: './literature.component.html',
  styleUrls: ['./literature.component.scss']
})
export class LiteratureComponent implements OnInit {
  @Input() posts: PostsInt[];

  constructor() {}

  ngOnInit() {}
}
