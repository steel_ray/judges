import { Component, OnInit, Input } from '@angular/core';
import { PostsInt } from 'src/app/core/interfaces/posts.int';
import { groupBy } from 'src/app/core/utility/helpers';

@Component({
  selector: 'ln-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.scss']
})
export class AuthorsComponent implements OnInit {
  @Input() posts: PostsInt[];
  authors: [] = [];
  authorsSelectOptions = [];
  selectedOpt: string;
  constructor() {}

  ngOnInit() {
    this.authors = groupBy(this.posts, 'author_name');
    this.authorsSelectOptions = Object.keys(this.authors);
    this.selectedOpt = this.authorsSelectOptions[0];
    // console.log(this.authorsSelectOptions);
  }

  onSelectChange(e) {
    this.selectedOpt = e.value;
  }
}
