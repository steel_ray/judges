import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material/tabs';

import { LibraryRoutingModule } from './library-routing.module';
import { LibraryComponent } from './library.component';
import { AuthorsComponent } from './components/authors/authors.component';
import { FilesComponent } from './components/files/files.component';
import { KeywordsComponent } from './components/keywords/keywords.component';
import { LiteratureComponent } from './components/literature/literature.component';
import { SidebarModule } from 'src/app/ui/sidebar/sidebar.module';
import { PageHeaderModule } from 'src/app/ui/page-header/page-header.module';
import { LayoutModule } from 'src/app/ui/layout/layout.module';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from 'src/app/core/shared/shared.module';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [
    LibraryComponent,
    AuthorsComponent,
    FilesComponent,
    KeywordsComponent,
    LiteratureComponent
  ],
  imports: [
    SharedModule,
    LibraryRoutingModule,
    SidebarModule,
    PageHeaderModule,
    MatTabsModule,
    LayoutModule,
    MatPaginatorModule,
    MatSelectModule
  ]
})
export class LibraryModule {}
