import { Component } from '@angular/core';
import { AppTranslateInit } from './app-translates-init';

@Component({
  selector: 'ln-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private translateInit: AppTranslateInit) {
    translateInit.init();
  }
}
