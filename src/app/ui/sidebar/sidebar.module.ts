import { NgModule } from '@angular/core';

import { SidebarComponent } from './sidebar.component';
import { MenuUrlModule } from 'src/app/core/pipes/menu-url/menu-url.module';

import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [SidebarComponent],
  imports: [SharedModule, MenuUrlModule],
  exports: [SidebarComponent]
})
export class SidebarModule {}
