import { Component, Input } from '@angular/core';
import { MenuInt } from 'src/app/core/interfaces/menu.int';

@Component({
  selector: 'ln-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  @Input() items: MenuInt[];
}
