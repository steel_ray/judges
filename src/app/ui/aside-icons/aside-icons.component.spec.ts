import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsideIconsComponent } from './aside-icons.component';

describe('AsideIconsComponent', () => {
  let component: AsideIconsComponent;
  let fixture: ComponentFixture<AsideIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsideIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsideIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
