import { NgModule } from '@angular/core';
import { AsideIconsComponent } from './aside-icons.component';
import { ToggleBlockModule } from 'src/app/core/directives/toggle-block/toggle-block.module';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [AsideIconsComponent],
  imports: [SharedModule, ToggleBlockModule],
  exports: [AsideIconsComponent]
})
export class AsideIconsModule {}
