import { Component } from '@angular/core';
import {
  APP_CONFIG,
  setDefaultLang
} from 'src/app/core/services/share/app-config-store.service';
import { TranslateService } from '@ngx-translate/core';
declare var $: any;
@Component({
  selector: 'ln-aside-icons',
  templateUrl: './aside-icons.component.html',
  styleUrls: ['./aside-icons.component.scss']
})
export class AsideIconsComponent {
  locales: any;
  activeLocale: any;
  voiceActive: boolean;
  constructor(private translate: TranslateService) {
    this.voiceActive = APP_CONFIG.specialAbs.textSpeech;
    this.locales = APP_CONFIG.data.locales;
    this.activeLocale = this.locales.find(
      l => l.locale === APP_CONFIG.data.defaultLocale
    );
    this.setSiteTheme();
  }

  onLocaleChange(locale) {
    this.translate.use(locale.locale);
    this.activeLocale = locale;
    setDefaultLang(locale.locale);
    // console.log(APP_CONFIG.data.defaultLocale);
    // localStorage.setItem('LN_LOCALE', locale.locale);
  }

  onVoiceActive() {
    this.voiceActive = !this.voiceActive;
    APP_CONFIG.specialAbs.textSpeech = this.voiceActive;
    return false;
  }
  onThemeChange(theme: string) {
    APP_CONFIG.specialAbs.siteTheme = theme;
    this.setSiteTheme();
  }
  setSiteTheme() {
    document.querySelector('html').className = '';
    document.querySelector('html').className = APP_CONFIG.specialAbs.siteTheme;
  }
}
