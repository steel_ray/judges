import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MenuInt } from 'src/app/core/interfaces/menu.int';
import { MenuService } from 'src/app/core/services/api/menu.service';
import { Observable, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ln-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit, OnDestroy {
  @Input() theme = 'default';
  items$: Observable<MenuInt[]>;
  unsub$ = new Subject<void>();
  constructor(
    private menuService: MenuService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.loadData();
    this.translate.onLangChange
      .pipe(takeUntil(this.unsub$))
      .subscribe(res => setTimeout(_ => this.loadData()));
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  loadData() {
    this.items$ = this.menuService.getItems();
  }
}
