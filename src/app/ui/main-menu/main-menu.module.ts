import { NgModule } from '@angular/core';

import { MainMenuComponent } from './main-menu.component';
import { GridModule } from 'src/app/core/directives/grid/grid.module';

import { MenuToggleModule } from 'src/app/core/directives/menu-toggle/menu-toggle.module';
import { MenuUrlModule } from 'src/app/core/pipes/menu-url/menu-url.module';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [MainMenuComponent],
  imports: [SharedModule, GridModule, MenuToggleModule, MenuUrlModule],
  exports: [MainMenuComponent]
})
export class MainMenuModule {}
