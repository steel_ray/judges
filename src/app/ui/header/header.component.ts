import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuService } from 'src/app/core/services/api/menu.service';
import { Observable, Subject } from 'rxjs';
import { MenuInt } from 'src/app/core/interfaces/menu.int';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'ln-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  topMenu$: Observable<MenuInt[]>;
  unsub$ = new Subject<void>();
  constructor(
    private menuService: MenuService,
    private translate: TranslateService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loadData();
    this.translate.onLangChange
      .pipe(takeUntil(this.unsub$))
      .subscribe(res => setTimeout(_ => this.loadData()));
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  loadData() {
    this.topMenu$ = this.menuService.getItems('top-menu');
  }

  onSearch(q: string) {
    document.querySelector('body').classList.remove('overflowed');
    this.router.navigate(['/search'], { queryParams: { q } });
    return false;
  }
}
