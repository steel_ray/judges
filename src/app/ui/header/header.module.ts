import { NgModule } from '@angular/core';
import { HeaderComponent } from './header.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MenuToggleModule } from 'src/app/core/directives/menu-toggle/menu-toggle.module';
import { FiexedMenuDirectiveModule } from 'src/app/core/directives/fixed-menu/fixed-menu-directive.module';
import { MainMenuModule } from '../main-menu/main-menu.module';
import { MenuUrlModule } from 'src/app/core/pipes/menu-url/menu-url.module';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    SharedModule,
    MatFormFieldModule,
    MatInputModule,
    MenuToggleModule,
    FiexedMenuDirectiveModule,
    MainMenuModule,
    MenuUrlModule
  ],
  exports: [HeaderComponent]
})
export class HeaderModule {}
