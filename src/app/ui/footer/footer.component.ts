import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PostsInt } from 'src/app/core/interfaces/posts.int';
import { PostService } from 'src/app/core/services/api/post.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { PostInt } from 'src/app/core/interfaces/post.int';

@Component({
  selector: 'ln-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnDestroy {
  usefulLinks$: Observable<PostsInt[]>;
  limit = 4;
  unsub$ = new Subject<void>();
  contacts$: Observable<PostInt>;
  nets$: Observable<PostInt>;
  constructor(
    private postService: PostService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.loadData();
    this.translate.onLangChange
      .pipe(takeUntil(this.unsub$))
      .subscribe(res => setTimeout(_ => this.loadData()));
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  loadData() {
    this.usefulLinks$ = this.postService.fetchAll({
      category_slug: 'useful-links',
      order: 'popular',
      limit: this.limit
    });
    this.contacts$ = this.postService.fetchOne('footer-contacts');
    this.nets$ = this.postService.fetchOne('footer-nets');
  }
}
