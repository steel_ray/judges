import { NgModule } from '@angular/core';

import { FooterComponent } from './footer.component';

import { MainMenuModule } from '../main-menu/main-menu.module';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [FooterComponent],
  imports: [SharedModule, MainMenuModule],
  exports: [FooterComponent]
})
export class FooterModule {}
