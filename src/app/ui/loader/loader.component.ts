import {
  Component,
  OnInit,
  OnDestroy,
  AfterContentChecked
} from '@angular/core';
import { Subject } from 'rxjs';
import { LoaderService } from 'src/app/core/services/api/loader.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ln-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit, OnDestroy {
  color = 'primary';
  mode = 'indeterminate';
  value = 50;
  unsub$ = new Subject<void>();
  isLoading = false;
  constructor(private loaderService: LoaderService) {}
  ngOnInit() {
    this.loaderService.isLoading
      .pipe(takeUntil(this.unsub$))
      .subscribe(res => setTimeout(_ => (this.isLoading = res)));
  }
  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }
}
