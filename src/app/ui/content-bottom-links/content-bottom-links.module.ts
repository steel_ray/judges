import { NgModule } from '@angular/core';
import { ContentBottomLinksComponent } from './content-bottom-links.component';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [ContentBottomLinksComponent],
  imports: [SharedModule],
  exports: [ContentBottomLinksComponent]
})
export class ContentBottomLinksModule {}
