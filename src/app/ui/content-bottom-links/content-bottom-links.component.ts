import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PostsInt } from 'src/app/core/interfaces/posts.int';
import { PostService } from 'src/app/core/services/api/post.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ln-content-bottom-links',
  templateUrl: './content-bottom-links.component.html',
  styleUrls: ['./content-bottom-links.component.scss']
})
export class ContentBottomLinksComponent implements OnInit, OnDestroy {
  posts$: Observable<PostsInt[]>;
  limit = 3;
  unsub$ = new Subject<void>();
  constructor(
    private postService: PostService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.loadData();
    this.translate.onLangChange
      .pipe(takeUntil(this.unsub$))
      .subscribe(res => setTimeout(_ => this.loadData()));
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  loadData() {
    this.posts$ = this.postService.fetchAll(
      {
        category_slug: 'perechen-dokumentov',
        limit: this.limit
      },
      true
    );
  }
  trackByFn(index, item) {
    return !item ? null : index;
  }
}
