import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { HeaderModule } from '../header/header.module';
import { FooterModule } from '../footer/footer.module';
import { ContentBottomLinksModule } from '../content-bottom-links/content-bottom-links.module';
import { AsideIconsModule } from '../aside-icons/aside-icons.module';
import { LoaderModule } from '../loader/loader.module';

@NgModule({
  declarations: [LayoutComponent],
  imports: [
    CommonModule,
    HeaderModule,
    FooterModule,
    ContentBottomLinksModule,
    AsideIconsModule,
    LoaderModule
  ],
  exports: [LayoutComponent]
})
export class LayoutModule {}
