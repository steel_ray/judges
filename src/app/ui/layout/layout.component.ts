import { Component, Input } from '@angular/core';

@Component({
  selector: 'ln-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent {
  @Input() mainPage = false;
}
