import { NgModule } from '@angular/core';
import { PageHeaderComponent } from './page-header.component';

import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [PageHeaderComponent],
  exports: [PageHeaderComponent],
  imports: [SharedModule]
})
export class PageHeaderModule {}
