import { Component, Input } from '@angular/core';
import { PostsInt } from 'src/app/core/interfaces/posts.int';

@Component({
  selector: 'ln-default-articles-list',
  templateUrl: './default-articles-list.component.html',
  styleUrls: ['./default-articles-list.component.scss']
})
export class DefaultArticlesListComponent {
  @Input() posts: PostsInt[];
}
