import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultArticlesListComponent } from './default-articles-list.component';

describe('DefaultArticlesListComponent', () => {
  let component: DefaultArticlesListComponent;
  let fixture: ComponentFixture<DefaultArticlesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultArticlesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultArticlesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
