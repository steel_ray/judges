import { NgModule } from '@angular/core';
import { DefaultArticlesListComponent } from './default-articles-list.component';
import { SharedModule } from 'src/app/core/shared/shared.module';

@NgModule({
  declarations: [DefaultArticlesListComponent],
  imports: [SharedModule],
  exports: [DefaultArticlesListComponent]
})
export class DefaultArticlesListModule {}
