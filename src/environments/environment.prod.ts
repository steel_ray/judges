export const environment = {
  production: true,
  apiEndpoint: '//server.school.webmaster.uz/api',
  langConst: 'LN_LOCALE',
  appDefaultLang: 'ru',
  pageSize: 10
};
